<?php

class Features extends CI_Controller
{
	public $CIF = NULL;

	public function __construct()
	{

		parent::__construct();

		$this->load->helper('url_helper');

		// Load form helper library
		$this->load->helper('form');

		$this->load->helper('file');

		// Load form validation library
		$this->load->library('form_validation');

		// Load session library
		$this->load->library('session');

		// Load database
		$this->load->model('login_database');
	}

	public function index()
	{

		$data['logiran'] = isset($this->session->userdata['logged_in']);

		if (!isset($this->session->userdata['logged_in'])) {
			$data['message_display'] = 'Signin to view this page!';
			$this->load->view('templates/header', $data);
			$this->load->view('user_authentication/login_form', $data);
			$this->load->view('templates/footer');
			return;
		}
		$data['logiran'] = isset($this->session->userdata['logged_in']);

		$videos = $this->getAllVidsfromone();
		$this->load->view('templates/header', $data);
		$this->load->view('features/members_area', ['videos' => $videos]);
		$this->load->view('templates/footer');
	}

	public function upload_video()
	{


		$this->form_validation->set_rules('title', 'Title', 'trim|required');
		$this->form_validation->set_rules('topic', 'Topic', 'trim|required');
		$this->form_validation->set_rules('product', 'Product', 'trim|required');

		if ($this->form_validation->run() === FALSE) {

			$this->index();

		} else {

			$data = array(
				'title' => $this->input->post('title'),
				'topic' => $this->input->post('topic'),
				'product' => $this->input->post('product'),
			);

			$config = [
				'upload_path' => './uploads',
				'allowed_types' => 'avi|mov|mp4',
			];

			$this->load->library('upload', $config);

			$this->form_validation->set_error_delimiters();

			if ($this->upload->do_upload('filename')) {
				$data = $this->input->post();
				$info = $this->upload->data();

				$vid_path = "./uploads/" . $info['raw_name'] . $info['file_ext'];

				$data['location'] = $vid_path;
				$data['user_id'] = $this->session->userdata['logged_in']['email'];
				unset($data['submit']);

				if ($this->login_database->insertvid($data)) {
					$this->index();
				} else {
					$this->index();
				}
			} else {
				$this->index();
			}
		}

	}

	public function najdi($data)
	{
		return $this->login_database->getUser($data);
	}

	public function getAllVidsfromone()
	{


		$user = $this->session->userdata['logged_in']['email'];
		$user_id = $this->login_database->getUserIDfromemail($user)[0]->id;

		return $this->login_database->getAllfromone($user_id);

	}

	public function delete($vidid)
	{

		$path = $this->login_database->findLocation($vidid)[0]->location;

		$u = $this->session->userdata['logged_in']['email'];
		$check = $this->login_database->getUserIDfromemail($u)[0]->id;
		$check2 = $this->login_database->getUserID($vidid)[0]->user_id;

		if ($check === $check2) {
			unlink($path);
			$this->login_database->delete_video($vidid);
			$this->index();
		} else {
			echo "You don't have permission to delete this item!";
		}

	}

	public function edit($id)
	{

		$data['logiran'] = isset($this->session->userdata['logged_in']);

		if (!isset($this->session->userdata['logged_in'])) {
			$data['message_display'] = 'Signin to view this page!';
			$this->load->view('templates/header', $data);
			$this->load->view('user_authentication/login_form', $data);
			$this->load->view('templates/footer');
			return;
		}

		$data['logiran'] = isset($this->session->userdata['logged_in']);

		$u = $this->session->userdata['logged_in']['email'];
		$check = $this->login_database->getUserIDfromemail($u)[0]->id;
		$check2 = $this->login_database->getUserID($id)[0]->user_id;

		if ($check === $check2) {

			$data['title'] = 'Update video post';

			$this->form_validation->set_rules('title', 'Title', 'required');
			$this->form_validation->set_rules('topic', 'Topic', 'required');

			if ($this->form_validation->run() === FALSE) {
				$video = $this->login_database->getVideos($id);

				$this->load->view('templates/header', $data);
				$this->load->view('features/edit', ['video' => $video]);
				$this->load->view('templates/footer');

			} else {
				$new["title"] = $this->input->post("title");
				$new["topic"] = $this->input->post("topic");
				$new["product"] = $this->input->post("product");
				$this->login_database->update_video($new, $id);
				$this->index();
			}
		} else {

			echo "You dont have permission to edit this video!";
		}
	}


}
