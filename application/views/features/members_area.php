<?php

if (!isset($this->session->userdata['logged_in'])) {
	$data['message_display'] = 'Signin to view this page!';
	$this->load->view('user_authentication/login_form', $data);
	return;
} ?>

<div class="row">
	<div class="col">
		<?php if (count($videos)): ?>
			<?php foreach ($videos as $vid): ?>
				<div>
					<div>
						<!-- Post Content Column -->
						<div>
							<!-- Title -->
							<h1 class="mt-4"><?php echo $vid->title; ?></h1>
							<p class="lead">
								Topic: <?php echo $vid->topic; ?>
							</p>
							<p class="lead">
								Product: <?php echo $vid->product; ?>
							</p>
							<!-- Author -->
							<p class="lead">
								by
								<?php
								$CIF = &get_instance();
								$fullname = $CIF->najdi($vid);
								echo $fullname[0]->name . " " . $fullname[0]->surname ?>
							</p>
							<hr>
							<!-- Date/Time -->
							<div class="row">
								<div class="col">
									<p><?php echo $vid->date; ?></p>
								</div>
								<div class="col">
									<div class="dropdown" style="float: right">
										<button class="btn btn-secondary dropdown-toggle" type="button"
												id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
												aria-expanded="false">
											Options
										</button>
										<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
											<a class="dropdown-item"
											   href="<?php echo site_url('features/edit/' . $vid->id) ?>">Edit</a>
											<a class="dropdown-item"
											   onclick='return confirm("Are you sure you want to delete?");'
											   href="<?php echo site_url('features/delete/' . $vid->id); ?>">Delete</a>
										</div>
									</div>
								</div>
							</div>
							<hr>
							<video width="320" height="240" controls>
								<source src="<?php echo base_url() . $vid->location; ?>" type="video/mp4">
								<source src="<?php echo base_url() . $vid->location; ?>" type="video/ogg">
							</video>
						</div>
					</div>
					<hr>
				</div>
			<?php endforeach; ?>
		<?php else: ?>
			<div class="alert alert-primary" role="alert">
				You have no videos currently in the database!
			</div>
		<?php endif; ?>
	</div>
	<div class="col">
		<div class="col">
			<div class="block-heading" align="center">
				<h2 class="text-info">Upload</h2>
				<p><?php echo "<div class='error_msg'>";
					echo validation_errors();
					echo "</div>";
					if (isset($error_message)) {
						echo $error_message;
					}; ?></p>
			</div>
		</div>

		<?php echo form_open_multipart('features/upload_video') ?>
		<div class="form-group"><label for="filename">Video</label>
			<?php
			$data1 = array(
					'type' => 'file',
					'name' => 'filename',
					'class' => 'form-control-file'
			);
			echo form_upload($data1); ?>
			<br/>
		</div>
		<div class="form-group"><label for="title">Title</label>
			<br/>
			<?php
			$data2 = array(
					'type' => 'text',
					'name' => 'title',
					'class' => 'form-control item'
			);
			echo form_input($data2); ?>
			<br/>
		</div>
		<div class="form-group"><label for="topic">Topic</label>
			<br/>
			<?php
			$data3 = array(
					'type' => 'text',
					'name' => 'topic',
					'class' => 'form-control item'
			);
			echo form_input($data3); ?>
			<br/>
		</div>
		<div class="form-group"><label for="title">Product</label>
			<br/>
			<?php
			$data4 = array(
					'type' => 'text',
					'name' => 'product',
					'class' => 'form-control item'
			);
			echo form_input($data4); ?>
			<br/>
		</div>
		<?php
		$data5 = array(
				'type' => 'submit',
				'name' => 'submit',
				'class' => 'btn btn-primary btn-block',
				'value' => 'Upload',
		);
		echo form_submit($data5);
		echo form_close();
		?>
	</div>
</div>

</body>
