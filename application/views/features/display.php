<div class="row justify-content-center">
	<div class="col-12 col-md-10 col-lg-8">
		<div class="card card-sm">
			<div class="card-body row no-gutters align-items-center">
				<div class="col-auto">
					<i class="fa fa-search h4 text-body"></i>
				</div>
				<!--end of col-->
				<div class="col">
					<input class="form-control form-control-lg form-control-borderless" id="textarea" type="search"
						   placeholder="Search topics or keywords">
				</div>
				<!--end of col-->
				<div class="col-auto">
					<button class="btn btn-lg btn-success" id="searchbutton" type="submit">Search</button>
				</div>
				<!--end of col-->
			</div>
		</div>
	</div>
	<!--end of col-->
</div>
<div class="row">
	<div class="col">
		<?php if (count($data['videos'])) : ?>
			<?php foreach ($data['videos'] as $vid) : ?>
				<div>
					<!-- Post Content Column -->
					<div>
						<!-- Title -->
						<h1 class="mt-4"><?php echo $vid['title']; ?></h1>
						<p class="lead">
							Topic: <?php echo $vid['topic']; ?>
						</p>
						<p class="lead">
							Product: <?php echo $vid['product']; ?>
						</p>
						<!-- Author -->
						<p class="lead">
							by <?php echo $vid['name'] . " " . $vid['surname']; ?>
						</p>
						<hr>
						<!-- Date/Time -->
						<div class="row">
							<div class="col">
								<p><?php echo $vid['date']; ?></p>
							</div>
						</div>
						<hr>

						<div class="">
							<div class="row">
								<div class="col">
									<video width="800" height="600" controls>
										<source src="<?php echo base_url() . $vid['location']; ?>" type="video/mp4">
									</video>

									<div class="post">
										<div class="post-action">
											<!-- Rating Bar -->
											<input id="post_<?= $vid['id'] ?>" value='<?= $vid['rating'] ?>'
												   class="rating-loading ratingbar" data-min="0" data-max="5"
												   data-step="1">
											<!-- Average Rating -->
											<div>Average Rating: <span
														id='averagerating_<?= $vid['id'] ?>'><?= $vid['averagerating'] ?></span>
											</div>
										</div>
									</div>
								</div>
								<div class="col">
									<div class="container d-flex justify-content-center mt-100 mb-100">
										<div class="row">
											<div class="col-md-12">
												<div class="card">
													<div class="card-body">
														<h4 class="card-title">Recent Comments</h4>
														<h6 class="card-subtitle">Latest Comments section by users</h6>
													</div>
													<div class="comment-widgets m-b-20"
														 style="overflow-y: scroll; height:400px; width: 400px;">
														<?php foreach ($vid['comments'] as $comment) : ?>
															<div class="d-flex flex-row comment-row">
																<div class="comment-text w-100">
																	<h5><?php echo $comment['name'] . " " . $comment['surname']; ?></h5>
																	<div class="comment-footer">
																		<span class="date">Posted on <?php echo $comment['date'] ?></span>
																		<span class="action-icons">
                                                                            <a class="commentedit"
																			   id="edit_<?= $comment['id'] ?>"
																			   data-abc="true"><i
																						class="fa fa-trash"></i></a>
                                                                        </span>
																	</div>
																	<p class="m-b-5 m-t-10"><?php echo $comment['text']; ?></p>
																</div>
															</div>
														<?php endforeach; ?>
														<hr>
													</div>
													<div>Leave a comment</div>
													<textarea class="textarea" id="text_<?= $vid['id'] ?>" name="text"
															  required></textarea>
													<input class="komentar" id="postcomment_<?= $vid['id'] ?>"
														   type="submit">
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<hr>
				</div>
			<?php endforeach; ?>
		<?php else : ?>
			<div class="alert alert-primary" role="alert">
				No such videos in the database!
			</div>
		<?php endif; ?>
	</div>
</div>
<!-- Script -->
<script type='text/javascript'>
	$(document).ready(function () {

		// Initialize
		$('.ratingbar').rating({
			showCaption: false,
			showClear: false,
			size: 'sm'
		});

		// Rating change
		$('.ratingbar').on('rating:change', function (event, value, caption) {
			var id = this.id;
			var splitid = id.split('_');
			var video_id = splitid[1];

			$.ajax({
				url: '<?= base_url() ?>index.php/star_rating_comment/updateRating',
				type: 'post',
				data: {
					video_id: video_id,
					rating: value,
				},
				success: function (response) {
					$('#averagerating_' + video_id).text(response);
				}
			});
		});
	});
	$(document).ready(function () {

		$('.komentar').click(function (event, value, caption) {

			var id = this.id;
			var splitid = id.split('_');
			var video_id = splitid[1];
			var text = $("#text_" + video_id).val();
			if (text == '') {
				alert("Please enter your comment");
			} else {
				$.ajax({
					url: '<?= base_url() ?>index.php/star_rating_comment/leave_comment',
					type: 'post',
					data: {
						video_id: video_id,
						text: text,
					},
					success: function (response) {
						setTimeout(function () {
							location.reload();
						}, 500);
					}
				});
			}
		})
	});

	$(document).ready(function () {

		$('#searchbutton').click(function (event, value, caption) {
			var text = $("#textarea").val();
			if (text == '') {
				alert("Please review your search parameters");
			} else {
				$.ajax({
					url: '<?= base_url() ?>index.php/star_rating_comment/searchfor',
					type: 'post',
					dataType: "html",
					data: {
						text: text,
					},
					success: function (response) {
						$('body').html(response);
					},
					error: function (result) {
						$('body').html("err");
					},
					beforeSend: function (d) {
						$('body').html("Searching...");
					}
				});
			}
		})
	});

	$(document).on("click", "a.commentedit", function () {

		var id = this.id;
		var splitid = id.split('_');
		var comment_id = splitid[1];
		$.ajax({
			url: '<?= base_url() ?>index.php/star_rating_comment/check',
			type: 'post',
			data: {
				comment_id: comment_id,
			},
			success: function (response) {
				if (response === 'true') {
					if (confirm('Are you sure ?')) {
						$.ajax({
							url: '<?= base_url() ?>index.php/star_rating_comment/delete_comment',
							type: 'post',
							data: {
								comment_id: comment_id,
							},
							success: function (response) {
								setTimeout(function () {
									location.reload();
								}, 500);
							}
						});
					}
				} else {
					alert("You dont have permission!");
				}
			}
		});

	});

</script>
