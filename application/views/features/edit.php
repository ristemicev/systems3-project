<div>
	<!-- Post Content Column -->
	<div>
		<div class="container">
			<div class="row">
				<div class="col">
					<h1 class="mt-4"><?php echo $video['title']; ?></h1>
					<hr>
					<p class="lead">
						Topic: <?php echo $video['topic']; ?>
					</p>
					<p class="lead">
						Product: <?php echo $video['product']; ?>
					</p>
					<hr>
					<!-- Date/Time -->
					<div class="row">
						<div class="col">
							<p><?php echo $video['date']; ?></p>
						</div>
					</div>
					<hr>
					<video width="320" height="240" controls>
						<source src="<?php echo base_url() . $video['location']; ?>" type="video/mp4">
						<source src="<?php echo base_url() . $video['location']; ?>" type="video/ogg">
						<!-- Comment with nested comments -->
				</div>
				<div class="col">
					<?php echo form_open('features/edit/'.$video['id']) ?>
					<div class="form-group"><label for="title">Title</label>
						<br/>
						<?php
						$data1 = array(
								'type' => 'text',
								'name' => 'title',
								'class' => 'form-control item',
						);
						echo form_input($data1); ?>
						<br/>
					</div>
					<div class="form-group"><label for="topic">Topic</label>
						<br/>
						<?php
						$data2 = array(
								'type' => 'text',
								'name' => 'topic',
								'class' => 'form-control item',
						);
						echo form_input($data2); ?>
						<br/>
					</div>
					<div class="form-group"><label for="title">Product</label>
						<br/>
						<?php
						$data4 = array(
								'type' => 'text',
								'name' => 'product',
								'class' => 'form-control item'
						);
						echo form_input($data4); ?>
						<br/>
					</div>
					<?php
					$data5 = array(
							'type' => 'submit',
							'name' => 'submit',
							'class' => 'btn btn-primary btn-block',
							'value' => 'Edit',
					);
					echo form_submit($data5);
					echo form_close();
					?>
				</div>
			</div>
		</div>
	</div>
</div>
