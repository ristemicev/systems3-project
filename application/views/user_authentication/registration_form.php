<main class="page registration-page">
	<section class="clean-block clean-form dark">
		<div class="container">
			<div class="block-heading">
				<h2 class="text-info">Registration</h2>
				<p><?php if (isset($message_display)) {
						echo $message_display;
					};
					echo "<div class='error_msg'>";
					echo validation_errors();
					echo "</div>";?></p>
			</div>
			<div>
				<div>
					<hr/>
					<?php

					echo form_open('user_authentication/signup');

					echo '<div class="form-group"><label for="name">Name</label>';
					echo "</br>";
					$data1 = array(
							'type' => 'text',
							'name' => 'name',
							'class' => 'form-control item'
					);
					echo form_input($data1);
					echo "<div class='error_msg'>";

					echo "</div>";
					echo "</br>";

					echo '<div class="form-group"><label for="surname">Surname</label>';
					echo "</br>";
					$data2 = array(
							'type' => 'text',
							'name' => 'surname',
							'class' => 'form-control item'
					);
					echo form_input($data2);
					echo "<div class='error_msg'>";

					echo "</div>";
					echo "<br/>";

					echo '<div class="form-group"><label for="email">Email</label>';
					echo "<br/>";
					$data3 = array(
							'type' => 'email',
							'name' => 'email_value',
							'class' => 'form-control item'
					);
					echo form_input($data3);
					echo "<br/>";
					echo '<div class="form-group"><label for="password">Password</label>';
					echo "<br/>";
					$data4 = array(
							'type' => 'password',
							'name' => 'password',
							'class' => 'form-control item'
					);
					echo form_password($data4);
					echo "<br/>";
					$data5 = array(
							'type' => 'submit',
							'name' => 'submit',
							'class' => 'btn btn-primary btn-block',
							'value' => 'Sign Up',
					);
					echo form_submit($data5);
					echo form_close();
					?>
					<a href="<?php echo base_url() ?>index.php/user_authentication/index">For Login Click Here</a>
				</div>
			</div>
		</div>
	</section>
</main>
