<?php

class Login_database extends CI_Model
{
	public function __construct()
	{
		$this->load->database();
	}

	// Insert registration data in database
	public function registration_insert($data)
	{

		// Query to check whether email already exist or not
		$condition = "email =" . "'" . $data['email'] . "'";
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where($condition);
		$this->db->limit(1);
		$query = $this->db->get();
		if ($query->num_rows() == 0) {

			// Query to insert data in database
			$this->db->insert('user', $data);
			if ($this->db->affected_rows() > 0) {
				return true;
			}
		} else {
			return false;
		}
	}

	// Read data using username and password
	public function login($data)
	{

		$condition = "email =" . "'" . $data['email'] . "' AND " . "password =" . "'" . $data['password'] . "'";
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where($condition);
		$this->db->limit(1);
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return true;
		} else {
			return false;
		}
	}

	// Read data from database to show data in admin page
	public function read_user_information($email)
	{

		$condition = "email =" . "'" . $email . "'";
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where($condition);
		$this->db->limit(1);
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return $query->result();
		} else {
			return false;
		}
	}

	public function insertvid($data)
	{

		$condition = "email =" . "'" . $data['user_id'] . "'";
		$this->db->select('id');
		$this->db->from('user');
		$this->db->where($condition);
		$this->db->limit(1);
		$query = $this->db->get();
		$data['user_id'] = $query->result()[0]->id;

		return $this->db->insert('video', $data);
	}

	public function delete_video($id)
	{
		$this->db->where("id", $id);
		$this->db->delete("video");
	}

	public function update_video($data, $id)
	{
		$this->db->set($data);
		$this->db->where("id", $id);
		$this->db->update("video");
	}

	public function getVideos($id = FALSE)
	{
		if ($id === FALSE) {
			$query = $this->db->get('video');
			if ($query->num_rows() > 0) {
				return $query->result();
			}
		}
		$query = $this->db->get_where('video', array('id' => $id));
		return $query->row_array();

	}

	public function getAllfromone($id)
	{
		$condition = "user_id = " . "'" . $id . "'";
		$this->db->select('*');
		$this->db->from('video');
		$this->db->where($condition);
		$this->db->order_by("id", "desc");
		$query = $this->db->get();

		return $query->result();
	}


	public function getUser($data)
	{
		$selection = "name, " . "surname";
		$condition = "id =" . "'" . $data->user_id . "'";
		$this->db->select($selection);
		$this->db->from('user');
		$this->db->where($condition);
		$this->db->limit(1);
		$query = $this->db->get();
		return $query->result();;
	}

	public function getUserID($data)
	{
		$condition = "id =" . "'" . $data . "'";
		$this->db->select('user_id');
		$this->db->from('video');
		$this->db->where($condition);
		$this->db->limit(1);
		$query = $this->db->get();

		return $query->result();
	}

	public function getUserIDfromemail($data)
	{

		$condition = "email =" . "'" . $data . "'";
		$this->db->select('id');
		$this->db->from('user');
		$this->db->where($condition);
		$this->db->limit(1);
		$query = $this->db->get();
		return $query->result();;
	}

	public function findLocation($id)
	{

		$condition = "id =" . "'" . $id . "'";
		$this->db->select('location');
		$this->db->from('video');
		$this->db->where($condition);
		$this->db->limit(1);
		$query = $this->db->get();
		return $query->result();

	}
}


