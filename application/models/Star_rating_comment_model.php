<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Star_rating_comment_model extends CI_Model
{

	public function __construct()
	{
		$this->load->database();
	}

	// Fetch records
	public function getAllPosts($userid)
	{

		// Posts
		$selection = 'v.id, v.title, v.date, v.topic, v.product, v.location, v.user_id, u.name, u.surname';
		$fromtables = 'video v, user u';
		$condition = 'v.user_id = u.id';
		$this->db->select($selection);
		$this->db->from($fromtables);
		$this->db->where($condition);
		$this->db->order_by("v.id", "desc");
		$postsquery = $this->db->get();

		$postResult = $postsquery->result_array();

		$posts_arr = array();
		foreach ($postResult as $post) {
			$id = $post['id'];
			$title = $post['title'];
			$date = $post['date'];
			$topic = $post['topic'];
			$product = $post['product'];
			$location = $post['location'];
			$user_id = $post['user_id'];
			$name = $post['name'];
			$surname = $post['surname'];

			// User rating
			$this->db->select('stars');
			$this->db->from('rating');
			$this->db->where("user_id", $userid);
			$this->db->where("video_id", $id);
			$userRatingquery = $this->db->get();

			$userpostResult = $userRatingquery->result_array();

			$userRating = 0;
			if (count($userpostResult) > 0) {
				$userRating = $userpostResult[0]['stars'];
			}

			// Average rating
			$this->db->select('ROUND(AVG(stars),1) as averageRating');
			$this->db->from('rating');
			$this->db->where("video_id", $id);
			$ratingquery = $this->db->get();

			$postResult = $ratingquery->result_array();

			$rating = $postResult[0]['averageRating'];

			if ($rating == '') {
				$rating = 0;
			}

			// Comments
			$condition = 'u.id = c.user_id AND c.video_id =' . $id;
			$this->db->select('name, surname, text, date, c.id');
			$this->db->from('user u, comment c');
			$this->db->where($condition);
			$commentsquery = $this->db->get();

			$commentsResult = $commentsquery->result_array();

			$posts_arr[] = array("id" => $id, "title" => $title, "topic" => $topic, "product" => $product, "location" => $location, "date" => $date, "user_id" => $user_id, "name" => $name, "surname" => $surname, "rating" => $userRating, "averagerating" => $rating, "comments" => $commentsResult);
		}

		return $posts_arr;
	}

	// Save user rating
	public function userRating($userid, $video_id, $rating)
	{
		$this->db->select('*');
		$this->db->from('rating');
		$this->db->where("video_id", $video_id);
		$this->db->where("user_id", $userid);
		$userRatingquery = $this->db->get();

		$userRatingResult = $userRatingquery->result_array();
		if (count($userRatingResult) > 0) {

			$postRating_id = $userRatingResult[0]['id'];
			// Update
			$value = array('stars' => $rating);
			$this->db->where('id', $postRating_id);
			$this->db->update('rating', $value);
		} else {
			$userRating = array(
				"video_id" => $video_id,
				"user_id" => $userid,
				"stars" => $rating
			);

			$this->db->insert('rating', $userRating);
		}

		// Average rating
		$this->db->select('ROUND(AVG(stars),1) as averageRating');
		$this->db->from('rating');
		$this->db->where("video_id", $video_id);
		$ratingquery = $this->db->get();

		$postResult = $ratingquery->result_array();

		$rating = $postResult[0]['averageRating'];

		if ($rating == '') {
			$rating = 0;
		}
		return $rating;
	}

	public function publish_comment($video_id, $user_id, $text)
	{

		$post_data = array(
			'video_id' => $video_id,
			'user_id' => $user_id,
			'text' => $text,
		);

		$this->db->insert('comment', $post_data);
	}

	public function searchvideos($userid, $keyword)
	{
		$selection = 'v.id, v.title, v.date, v.topic, v.product, v.location, v.user_id, u.name, u.surname';
		$fromtables = 'video v, user u';
		$condition = "v.user_id = u.id and (v.topic like '%" . $keyword . "%' or v.title like '%" . $keyword . "%' or v.product like '%" . $keyword . "%')";
		$this->db->select($selection);
		$this->db->from($fromtables);
		$this->db->where($condition);
		$postsquery = $this->db->get();

		$postResult = $postsquery->result_array();

		$posts_arr = array();
		foreach ($postResult as $post) {
			$id = $post['id'];
			$title = $post['title'];
			$date = $post['date'];
			$topic = $post['topic'];
			$product= $post['product'];
			$location = $post['location'];
			$user_id = $post['user_id'];
			$name = $post['name'];
			$surname = $post['surname'];

			// User rating
			$this->db->select('stars');
			$this->db->from('rating');
			$this->db->where("user_id", $userid);
			$this->db->where("video_id", $id);
			$userRatingquery = $this->db->get();

			$userpostResult = $userRatingquery->result_array();

			$userRating = 0;
			if (count($userpostResult) > 0) {
				$userRating = $userpostResult[0]['stars'];
			}

			// Average rating
			$this->db->select('ROUND(AVG(stars),1) as averageRating');
			$this->db->from('rating');
			$this->db->where("video_id", $id);
			$ratingquery = $this->db->get();

			$postResult = $ratingquery->result_array();

			$rating = $postResult[0]['averageRating'];

			if ($rating == '') {
				$rating = 0;
			}

			// Comments
			$condition = 'u.id = c.user_id AND c.video_id =' . $id;
			$this->db->select('name, surname, text, date, c.id');
			$this->db->from('user u, comment c');
			$this->db->where($condition);
			$commentsquery = $this->db->get();

			$commentsResult = $commentsquery->result_array();

			$posts_arr[] = array("id" => $id, "title" => $title, "topic" => $topic, "product" => $product, "location" => $location, "date" => $date, "user_id" => $user_id, "name" => $name, "surname" => $surname, "rating" => $userRating, "averagerating" => $rating, "comments" => $commentsResult);
		}

		return $posts_arr;
	}

	public function delete_comment($commentid)
	{

		$this->db->where("id", $commentid);
		$this->db->delete("comment");
	}

	public function getUserIDfromComment($data)
	{
		$condition = "id =" . "'" . $data . "'";
		$this->db->select('user_id');
		$this->db->from('comment');
		$this->db->where($condition);
		$this->db->limit(1);
		$query = $this->db->get();

		return $query->result();
	}
}
